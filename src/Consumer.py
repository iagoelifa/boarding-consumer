import pika
import json
import requests
import os
from schema import Schema, And, Use, Optional, SchemaError

class Consumer:

    schema = Schema({
        'busId': str,
        'cameraId': str,
        'date': int,
        'passengers': int,
        'linkFiles': list,
        'linkVideos': list,
        'beginVideo': int,
        'endVideo': int,
        'coordinates': str,
        'events': list
    })

    def __init__ (self, queue_name, queue_host, queue_port, queue_user, queue_password, api_boarding_addr, api_event_addr, api_path_addr, api_bus_addr, api_place_addr):
        
        self.queue_name = queue_name
        self.queue_host = queue_host
        self.queue_port = queue_port
        self.queue_user = queue_user
        self.queue_password = queue_password
        
        self.api_boarding_addr = api_boarding_addr
        self.api_event_addr = api_event_addr
        self.api_path_addr = api_path_addr
        self.api_bus_addr = api_bus_addr
        self.api_place_addr = api_place_addr
        
        self.connect()

    def find_2_nearest_places(self, coords, places):
        
        lat, lon = map(float, coords.split(' '))
        print (lat, lon)
        dists = {}
        
        for place in places:
            place_lat, place_lon = map(float, place['coordinates'].split(' '))
            dist = ((place_lat-lat)**2 + (place_lon-lon)**2)**(1/2)
            print (dist)
            dists[place['_id']] = dist

        print (sorted(dists, key=lambda x: dists[x]))
        return list(sorted(dists, key=lambda x: dists[x]))[:2] 

    def fetch_places(self, busId):
        
        buses = json.loads(requests.get(f'{self.api_bus_addr}/bus/').text)
        bus = next(filter(lambda x: x['_id'] == busId, buses))
        print (bus)

        places = json.loads(requests.get(f'{self.api_place_addr}/place/').text)
        print (places)

        return list(filter(lambda x: x['idLine'] == bus['lineId'], places))

    def find_path(self, nearest_places):

        paths = json.loads(requests.get(f'{self.api_path_addr}/path/').text)
        print (paths, nearest_places)
        return next(filter(lambda x: x['placeId'] == nearest_places[0] and  x['nextPlaceId'] == nearest_places[1] or x['placeId'] == nearest_places[0] and  x['nextPlaceId'] == nearest_places[1], paths))

    def consume(self):

        self.channel.start_consuming()
    
    def callback(self, ch, method, properties, body):
        
        try:
            payload = json.loads(body)
            
            events = payload['events']
            payload.pop('events')
            
            path_id = self.find_path(self.find_2_nearest_places(payload['coordinates'], self.fetch_places(payload['busId'])))['_id']
            
            res = json.loads(requests.post(f'{self.api_boarding_addr}/boarding/', json={**payload, 'pathId': path_id}).text)
            boarding_id = res['_id']
            
            for event in events:
                print (requests.post(f'{self.api_event_addr}/event/', json={**event, 'pathId':path_id, 'boardingId':boarding_id}))

        except SchemaError as e:
            print(e, e.args)
        except Exception as e:
            raise e

    def connect(self):

        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.queue_host,
                port=self.queue_port,
                credentials=pika.PlainCredentials(username=self.queue_user, password=self.queue_password)
            )
        )

        self.channel = connection.channel()

        self.channel.basic_consume(queue=self.queue_name, on_message_callback=self.callback, auto_ack=True)