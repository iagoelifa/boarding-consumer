import pika
import sys
import json
import time

# 'filter_task_queue', 'sflabs', 'b0l0d3c3n0ur4', '104.198.209.102', '80', 5
# credentials = pika.PlainCredentials(username='guest', password='guest')
# connection = pika.BlockingConnection(pika.ConnectionParameters('localhost', '5672',credentials=credentials))

credentials = pika.PlainCredentials(username='guest', password='guest')
connection = pika.BlockingConnection(pika.ConnectionParameters('35.196.60.209', '5672', credentials=credentials))

channel = connection.channel()
channel.queue_declare(queue='boarding', durable=True)

message = {'busId': '5f4a62fa66e6c16f6514ae4c', 'cameraId': '3', 'date': 1598725346.030259, 'passengers': 2, 'linkFiles': ['url1', 'url2'], 'coordinates': '-22.0149393 -47.8899033', 'linkVideos': ['url1'], 'beginVideo': 1231212431, 'endVideo': 1231234123, 'events': [{'type': 
'Pendency', 'description': 'Câmera 1 off', 'busId': '5f370f55bc2dc57f7350cede', 'cameraId': '3', 'date': 1598725346.030259, 'active': False}]}

channel.basic_publish(exchange='',
                      routing_key='boarding',
                      body=json.dumps(message))

print(" [x] Sent %r" % message)

connection.close()
