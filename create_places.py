import requests

lines = [
{
"idLine": "5f465a85348ee384e07e4753",
"description": "LOURDES",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "BARRO VERMELHO",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "CANHOBA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO AMPARO",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TELHA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "CEDRO",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "SÃO FRANCISCO",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO CRUZ",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO FEDERAL",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO PIRUNGA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO SANTA CLARA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO JAPARATUBA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO CARMÓPOLIS",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "TREVO VALE",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "ROSÁRIO",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "MARUIM",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "PEDRA BRANCA",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
},
{
"idLine": "5f465a85348ee384e07e4753",
"description": "ARACAJU",
"coordinates": "23123123 - 123123123",
"linkPhoto": "",
"tax": 20,
}
]

for line in lines:
    print (requests.post('http://34.70.151.227:3000/place', json=line))
