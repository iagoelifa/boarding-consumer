import json
import requests

places = requests.get("http://34.70.151.227:3000/place/")
a=json.loads(places.text)

for i in range(1,len(a)-1):
    path = {
        'lineId': a[i]["idLine"], 
        'placeId': a[i-1]["_id"], 
        'nextPlaceId': a[i]["_id"], 
        'first': False,
        '_v': 0,
        'description': a[i-1]["description"]+" - "+a[i]["description"],
        'tax': 10.50
    }

    print(json.dumps(path))
    # print(requests.post("http://34.68.204.47:3000/path/", json=json.dumps(path)).text.encode(encoding="utf-8"))